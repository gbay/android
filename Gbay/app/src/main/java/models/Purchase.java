package models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by fofofofodev on 19/04/2017.
 */

public class Purchase {

    private String comment;
    private double note;
    private String productName;
    private int quantity;
    private int id;
    private int sellId;

    public int getSellId() {
        return sellId;
    }

    public void setSellId(int sellId) {
        this.sellId = sellId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public double getNote() {
        return note;
    }

    public void setNote(double note) {
        this.note = note;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void initPurchaseFromJson(JSONObject jsonObject){


    }

}
