package models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by fofofofodev on 04/04/2017.
 */

public class Category {

    private int id;
    private String name;





    public void initFromJson(JSONObject jsonObject){
        try{
            id = jsonObject.getInt("id");
            name = jsonObject.getString("name");
        }catch(JSONException excep){
            excep.printStackTrace();
        }
    }


    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Category(){

    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
