package models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by fofofofodev on 02/04/2017.
 */

public class Auction {

    private int id;
    private Date startDate;
    private Date endDate;
    private int productId;
    private int quantity;
    private double basePrice;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public int getProductId() {

        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public ArrayList<Bid> getAuctionBids() {
        return auctionBids;
    }

    public void setAuctionBids(ArrayList<Bid> auctionBids) {
        this.auctionBids = auctionBids;
    }

    private ArrayList<Bid> auctionBids;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void initBids(JSONArray rawBids){

        auctionBids = new ArrayList<>();

        try{

            for(int i = 0; i< rawBids.length(); i++){
                JSONObject object = rawBids.getJSONObject(i);

                Bid bid = new Bid();

                bid.setId(object.getInt("id"));
                bid.setAmount(object.getDouble("amount"));
                bid.initBidder(object.getJSONObject("buyer"));

                auctionBids.add(bid);
            }


        }catch (JSONException e){



        }





    }
}
