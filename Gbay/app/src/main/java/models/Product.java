package models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by fofofofodev on 29/03/2017.
 */

public class Product  implements Parcelable{

    private String name;
    private String description;
    private double price;
    private boolean isDirectBuy;
    private int id;
    private Seller seller;
    private Auction auction;
    private ArrayList<Category> categories;
    private int stock;
    private int categoryId;

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    private ArrayList<Sell> productDoneSells;

    public ArrayList<Sell> getProductDoneSells() {
        return productDoneSells;
    }

    public void setProductDoneSells(ArrayList<Sell> productDoneSells) {
        this.productDoneSells = productDoneSells;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public ArrayList<Sell> getProductSellsWithCurrentAuctions() {
        return productSellsWithCurrentAuctions;
    }

    public void setProductSellsWithCurrentAuctions(ArrayList<Sell> productSellsWithCurrentAuctions) {
        this.productSellsWithCurrentAuctions = productSellsWithCurrentAuctions;
    }

    private ArrayList<Sell> productSellsWithCurrentAuctions;

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    private int sellerId;


    public Product(){}

    public void stringToCateg(String [] rawCategs){
        for (String categ: rawCategs) {
            Category tmpCateg = new Category();
            tmpCateg.setName(categ);
            if(categories.size() > 0){
                for (Category category : categories){
                    if(!category.getName().equals(categ)){
                        categories.add(tmpCateg);
                    }
                }
            }
            else{
                categories.add(tmpCateg);
            }
        }
    }


    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public void initSeller(JSONObject jsonObject){

        try{

            seller = new Seller();
            seller.setId(jsonObject.getInt("id"));
            seller.setPassword(jsonObject.getString("password"));
            seller.setEmail(jsonObject.getString("email"));
            seller.setName(jsonObject.getString("name"));
            seller.setCredits(jsonObject.getDouble("credits"));
            seller.setRoleId(jsonObject.getInt("roleId"));

        }catch(JSONException excep){

        }
    }

    public void initCatgeories(JSONArray jsonArray){

        categories = new ArrayList<>();
        try{
            int count = 0;
            while(count < jsonArray.length()){
                Category category = new Category();
                category.initFromJson(jsonArray.getJSONObject(count));
                categories.add(category);
                count ++;
            }
        }catch(JSONException excep){
            excep.printStackTrace();
        }



    }



    public void initProductSells(JSONArray jsonArray){

        productSells = new ArrayList<>();
        productSellsWithCurrentAuctions = new ArrayList<>();
        productDoneSells = new ArrayList<>();

        try
        {
            int count = 0;
            while(count < jsonArray.length()){

                JSONObject jsonSell = jsonArray.getJSONObject(count);

                Sell sell = new Sell();

                sell.setId(jsonSell.getInt("id"));
                sell.setQuantity(jsonSell.getInt("quantity"));
                sell.setProductId(jsonSell.getInt("productId"));

                if(!jsonSell.isNull("auction")){

                    Log.i("auction in Sell",jsonSell.getJSONObject("auction").toString());


                    sell.initAuction(jsonSell.getJSONObject("auction"));
                    Log.i("auctionbasePrice", String.valueOf(sell.getAuction().getBasePrice()));
                    Log.i("auctionQuantity", String.valueOf(sell.getAuction().getQuantity()));
                    productSellsWithCurrentAuctions.add(sell);

                }
                if(!jsonSell.isNull("buyerId") && !jsonSell.isNull("buyer")){
                        Seller sellBuyer = new Seller();
                        sellBuyer.setId(jsonSell.getInt("buyerId"));
                        sellBuyer.setName(jsonSell.getJSONObject("buyer").getString("name"));
                        sell.setBuyer(sellBuyer);
                        productDoneSells.add(sell);
                }
                productSells.add(sell);
                count ++;
            }
        }catch(JSONException excep){
            Log.i("Error initProductSells", excep.getMessage());
        }
    }

    public String CategsToString(){
        String result="";
        for(int i = 0; i < categories.size(); i++){
            result += categories.get(i).getName();
            if(i != categories.size() - 1){
                result += ",";
            }
        }
        return result;
    }

    public ArrayList<Sell> getProductSells() {
        return productSells;
    }

    public void setProductSells(ArrayList<Sell> productSells) {
        this.productSells = productSells;
    }

    private int auctionId;
    private ArrayList<Sell> productSells;

    public int getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(int auctionId) {
        this.auctionId = auctionId;
    }



    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    public boolean isDirectBuy() {
        return isDirectBuy;
    }

    public void setDirectBuy(boolean directBuy) {
        isDirectBuy = directBuy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public String getDescription() {
        return description;
    }

    public Product(String aName){
        this.name = aName;
        productSells = new ArrayList<>();
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    public  static final Parcelable.Creator<Product> CREATOR  = new Parcelable.Creator<Product>(){


        @Override
        public Product createFromParcel(Parcel source) {
            return null;
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }


    };

}
