package models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by fofofofodev on 15/04/2017.
 */

public class Bid {

    private int id;
    private int bidderId;
    private int auctionId;
    private double amount;
    private Seller bidder;

    public Seller getBidder() {
        return bidder;
    }

    public void setBidder(Seller bidder) {
        this.bidder = bidder;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBidderId() {
        return bidderId;
    }

    public void setBidderId(int bidderId) {
        this.bidderId = bidderId;
    }

    public int getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(int auctionId) {
        this.auctionId = auctionId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void initBidder(JSONObject rawBidder){

        bidder = new Seller();

        try {

            bidder.setId(rawBidder.getInt("id"));
            bidder.setName(rawBidder.getString("name"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
