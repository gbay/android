package models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by fofofofodev on 02/04/2017.
 */

public class Seller {

    private int id;
    private String name;
    private double credits;
    private String email;
    private String password;
    private int roleId;
    private ArrayList<Sell> userPurchases;
    private ArrayList<Bid> userBids;
    private ArrayList<Product> userProducts;
    private ArrayList<Evaluation> userEvaluations;
    private ArrayList<Category> sellerProductsCategories;






    public void awakeUserFromJson(JSONObject response){

        try {
            awakeUserPurchasesFromJson(response.getJSONArray("purchases"));
            credits = response.getDouble("credits");

            name = response.getString("name");

            fillUserProductsFromJson(response.getJSONArray("products"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void fillUserProductsFromJson(JSONArray response){
        userProducts = new ArrayList<>();
        try{
            int count = 0;

            while(count < response.length()){
                Product product = new Product();
                product.setId(response.getJSONObject(count).getInt("id"));
                product.setDirectBuy(Boolean.valueOf(response.getJSONObject(count).getString("directBuy")));
                product.setName(response.getJSONObject(count).getString("name"));
                product.setPrice(Double.valueOf(response.getJSONObject(count).getString("price")));
                product.setSellerId(Integer.valueOf(response.getJSONObject(count).getString("sellerId")));
                if(!response.getJSONObject(count).isNull("stock")){
                    product.setStock(response.getJSONObject(count).getInt("stock"));
                }
                product.setDescription(response.getJSONObject(count).getString("description"));
                if(!response.getJSONObject(count).isNull("categories")){product.initCatgeories(response.getJSONObject(count).getJSONArray("categories"));}

                //
                /*
                JSONArray categoriesArray = response.getJSONObject(count).getJSONArray("categories");
                if(categoriesArray.length() > 0){
                    sellerProductsCategories = new ArrayList<>();
                    for(int i = 0; i < categoriesArray.length(); i++){
                        Category category = new Category();
                        category.initFromJson(categoriesArray.getJSONObject(i));
                        sellerProductsCategories.add(category);
                    }
                }
                */

                userProducts.add(product);
                count ++;
            }
        }catch (JSONException excep){
            Log.i(getClass().getSimpleName(), excep.getMessage());
        }
    }


    public void awakeUserPurchasesFromJson(JSONArray response){
        userPurchases = new ArrayList<>();
        try{
            int count = 0;
            while(count < response.length()){

                Sell sell = new Sell();
                sell.setId(response.getJSONObject(count).getInt("id"));
                sell.setQuantity(response.getJSONObject(count).getInt("quantity"));
                sell.setBuyerId(response.getJSONObject(count).getInt("buyerId"));
                sell.setProductId(response.getJSONObject(count).getInt("productId"));
                userPurchases.add(sell);
                count++;
            }
        }catch(JSONException excep){
            Log.i("errorParsePurchases", excep.getMessage().toString());
        }

    }


    public ArrayList<Sell> getUserPurchases() {
        return userPurchases;
    }

    public void setUserPurchases(ArrayList<Sell> userPurchases) {
        this.userPurchases = userPurchases;
    }

    public ArrayList<Bid> getUserBids() {
        return userBids;
    }

    public void setUserBids(ArrayList<Bid> userBids) {
        this.userBids = userBids;
    }

    public ArrayList<Product> getUserProducts() {
        return userProducts;
    }

    public void setUserProducts(ArrayList<Product> userProducts) {
        this.userProducts = userProducts;
    }

    public ArrayList<Evaluation> getUserEvaluations() {
        return userEvaluations;
    }

    public void setUserEvaluations(ArrayList<Evaluation> userEvaluations) {
        this.userEvaluations = userEvaluations;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private String token;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCredits() {
        return credits;
    }

    public void setCredits(double credits) {
        this.credits = credits;
    }
}
