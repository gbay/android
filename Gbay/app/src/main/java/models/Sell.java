package models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by fofofofodev on 09/04/2017.
 */

public class Sell {

    private int id;
    private int quantity;
    private int productId;
    private int buyerId;
    private Auction auction;
    private int auctionId;
    private Seller buyer;

    public Seller getBuyer() {
        return buyer;
    }

    public void setBuyer(Seller buyer) {
        this.buyer = buyer;
    }

    public int getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(int auctionId) {
        this.auctionId = auctionId;
    }

    public void initAuction(JSONObject jsonObject){
        auction = new Auction();
        SimpleDateFormat formatter =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        if(jsonObject != null){
            try {

                this.auction.setId(Integer.valueOf(jsonObject.getString("id")));
                String tmpStart = jsonObject.getString("start");
                String tmpEnd = jsonObject.getString("end");
                this.auction.setStartDate(formatter.parse(tmpStart));
                this.auction.setEndDate(formatter.parse(tmpEnd));
                this.auction.setBasePrice(jsonObject.getDouble("basePrice"));

            } catch (JSONException e) {
                e.printStackTrace();
                Log.i("Error initAuction", e.getCause().toString());
            } catch (ParseException e) {
                e.printStackTrace();
                Log.i("Error initAuction", e.getCause().toString());
            }
        }

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }
}
