package Tasks;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.fofofofodev.gbay.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import models.Login;
import models.Seller;

/**
 * Created by fofofofodev on 08/04/2017.
 */

public class AuthTask {

    private String token;
    private String reqForTokenUrl = "https://gbay.bmyguest.ovh/auth/login";
    private String registerUrl = "https://gbay.bmyguest.ovh/auth/register";

    Context context;


    Login mLogin;


    public AuthTask(Context ctx){
        context = ctx;
    }


    public void requestForToken(final Login login, final getTokenCallBack aCallBack){

        final JSONObject reqJson = new JSONObject();

        try {
            reqJson.put("email", login.getEmail().toString());
            reqJson.put("password", login.getPassword().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, reqForTokenUrl, reqJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("raw Token", response.toString());

                        try{
                            Seller seller = new Seller();
                            seller.setId(response.getInt("userId"));
                            seller.setToken(response.getString("token"));

                            aCallBack.onSuccess(seller);

                        }catch(JSONException excep){
                            aCallBack.onFail("Echec parsing logged user");

                        }
                    }

                    }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                            aCallBack.onFail("Echec connexion API pour auth/login");
                    }
        });

        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }




    public void register(Seller  newUser, final registerCallBack aCallBack){

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", newUser.getEmail());
            jsonObject.put("password", newUser.getPassword());
            jsonObject.put("name", newUser.getName());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, registerUrl, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("Raw new user", response.toString());

                        if(response != null){
                            aCallBack.onSuccess(true);
                        }
                        else {
                            aCallBack.onFail(false);
                        }
                    }
                }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("register fail", error.getMessage().toString());
                    aCallBack.onFail(false);
                }
        });

        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }




    public interface getTokenCallBack{
        void onSuccess(Seller seller);
        void onFail(String msg);
    }


    public interface registerCallBack{
        void onSuccess(boolean result);
        void onFail(boolean result);
    }



}
