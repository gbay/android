package Tasks;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.fofofofodev.gbay.MySingleton;
import com.example.fofofofodev.gbay.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import models.Auction;
import models.Category;
import models.Product;
import models.Sell;

/**
 * Created by fofofofodev on 10/04/2017.
 */

public class ProductTask {
    private Context ctx;

    Product product;

    ArrayList<Sell> productsCurrentAuctions = new ArrayList<>();


    public ProductTask(Context aContext){
        ctx = aContext;
    }

    String rootReqUrl = "https://gbay.bmyguest.ovh";

    public void buyProduct(Sell sell, final String token, final getProductByIdCallBack aCallback){
        String req = rootReqUrl + "/products/"+sell.getProductId()+"/buy";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("quantity", sell.getQuantity());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, req, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                initProductFromJson(response);
                aCallback.onSuccess(product);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization",token);
                return headers;
            }
        };

        MySingleton.getInstance(ctx).addToRequestQueue(request);
    }

    public void initProductFromJson(JSONObject response){
        try{
            if (product != null) {product = null;}
            product = new Product(response.getString("name"));
            product.setId(response.getInt("id"));
            product.setDescription(response.getString("description"));
            product.setSellerId(response.getInt("sellerId"));
            product.setPrice(response.getDouble("price"));
            product.initProductSells(response.getJSONArray("sells"));

            product.initCatgeories(response.getJSONArray("categories"));

            product.initSeller(response.getJSONObject("seller"));
            product.setDirectBuy(response.getBoolean("directBuy"));
            if(!response.isNull("stock")){product.setStock(response.getInt("stock"));}
        }catch(JSONException excep){
            Log.i(getClass().getName(), excep.toString());
        }
    }

    public Auction initAuction(JSONObject jsonObject){
        Auction auction = null;
        SimpleDateFormat formatter =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        if(jsonObject != null){
            auction = new Auction();
            try {
                auction.setId(Integer.valueOf(jsonObject.getString("id")));
                String tmpStart = jsonObject.getString("start");
                String tmpEnd = jsonObject.getString("end");
                auction.setStartDate(formatter.parse(tmpStart));
                auction.setEndDate(formatter.parse(tmpEnd));
                auction.setQuantity(jsonObject.getInt("quantity"));
                auction.setBasePrice(jsonObject.getDouble("basePrice"));
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return auction;
    }


    public void getProductById(String productId, final getProductByIdCallBack aCallaback){
        String reqUrl = rootReqUrl + "/products/"+ productId;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, reqUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        initProductFromJson(response);
                        aCallaback.onSuccess(product);
                    }
                }
                , new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ctx, "Unable to connect for product By Id", Toast.LENGTH_LONG).show();
                    }

        });

        MySingleton.getInstance(ctx).addToRequestQueue(jsonObjectRequest);
    }

    public void updateProduct(final String token, Product aProduct, final getProductByIdCallBack aCallback){
        product = aProduct;
        String req = rootReqUrl + "/products/" + product.getId();
        JSONObject reqBody = new JSONObject();
        try{
            reqBody.put("name",product.getName());
            reqBody.put("description", product.getDescription());
            reqBody.put("price",String.valueOf( product.getPrice()));
            reqBody.put("directBuy", String.valueOf(product.isDirectBuy()));
            reqBody.put("stock", String.valueOf(product.getStock()));
            //reqBody.put("categoryId", String.valueOf(product.getCategoryId()));
            JSONArray categsArray = new JSONArray();
            for(Category category: product.getCategories()){
                categsArray.put(String.valueOf(category.getId()));
            }
            reqBody.put("categories", categsArray);
        }catch (JSONException excep){
            Log.i(getClass().getName(), excep.getMessage());
        }


        Log.i("updateProduct", reqBody.toString());



        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, req, reqBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response){
                        initProductFromJson(response);
                        aCallback.onSuccess(product);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization",token);
                return headers;
            }
        };

        MySingleton.getInstance(ctx).addToRequestQueue(jsonObjectRequest);

    }

    public void deleteProduct(final String token, String productId, final updateProductCallback callback){

        String reqUrl = rootReqUrl + "/products/"+ productId;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, reqUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onSucces();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization",token);
                return headers;
            }
        };

        MySingleton.getInstance(ctx).addToRequestQueue(jsonObjectRequest);
    }


    public interface getProductByIdCallBack{
        void onSuccess(Product product);
        void onFail(String msg);
    }

    public interface updateProductCallback{
        void onSucces();
        void onFail();
    }

    public interface productAuctionsCallBack{
        void onSucces(ArrayList<Sell> sells);
    }
}
