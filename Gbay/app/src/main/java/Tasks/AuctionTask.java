package Tasks;

import android.content.Context;
import android.provider.SyncStateContract;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.fofofofodev.gbay.MySingleton;
import com.example.fofofofodev.gbay.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import models.Auction;
import models.Bid;
import models.Product;
import models.Sell;

/**
 * Created by fofofofodev on 15/04/2017.
 */

public class AuctionTask {


    Context context;

    String srcReq = "https://gbay.bmyguest.ovh/";

    Auction auction;

    ArrayList<Sell> allAuctions = new ArrayList<>();



    public AuctionTask(Context aContext){
        context = aContext;
    }

    public void makeABid(final Auction auction, String bidAmount, final String token, final getAuctionCallBack aCallBack){

        String url = "https://gbay.bmyguest.ovh/auctions/"+auction.getId()+"/bids";

        JSONObject reqBody = new JSONObject();

        try {
            reqBody.put("amount", Double.valueOf(bidAmount));
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url , reqBody,

                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.i(getClass().getName(), response.toString());
                            //initBidFromJson(response);
                            aCallBack.onSucces(auction);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i(getClass().getName(), error.toString());
                }
            }){

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return super.getParams();
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Authorization",token);
                    return headers;
                }
            };

            MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void initBidFromJson(JSONObject response){

        try {

            Bid bid = new Bid();
            bid.setId(response.getInt("id"));
            bid.setAmount(response.getDouble("amount"));
            bid.initBidder(response.getJSONObject("buyer"));
            auction.getAuctionBids().add(bid);
        }catch(JSONException excep){
            Log.i(getClass().getName(), excep.toString());

        }
    }


    public void getAuctionsBid(int auctionId, final getAuctionCallBack aCallBack){
        String req = srcReq  + "auctions/" + String.valueOf(auctionId);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, req, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                 auction = new Auction();
                try {
                    SimpleDateFormat formatter =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    auction.setId(response.getInt("id"));
                    String tmpStart = response.getString("start");
                    String tmpEnd = response.getString("end");
                    auction.setStartDate(formatter.parse(tmpStart));
                    auction.setEndDate(formatter.parse(tmpEnd));

                    if(!response.isNull("bids")){
                        auction.initBids(response.getJSONArray("bids"));
                    }

                    aCallBack.onSucces(auction);


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("failed auctionBids", error.toString());
            }
        });
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }


    public void getAllAuctions(final getAllAuctionsCallBack aCallBack){

        String reqUrl = srcReq + "auctions";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, reqUrl, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try{
                            int count = 0;

                            SimpleDateFormat formatter =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

                            while(count < response.length()){

                                if(response.getJSONObject(count).isNull("closedAt")){

                                    Sell sell = new Sell();

                                    sell.setId(Integer.valueOf(response.getJSONObject(count).getString("id")));

                                    Auction rawAuction = new Auction();


                                    String tmpStart = response.getJSONObject(count).getString("start");
                                    String tmpEnd = response.getJSONObject(count).getString("end");
                                    try {
                                        rawAuction.setStartDate(formatter.parse(tmpStart));
                                        rawAuction.setEndDate(formatter.parse(tmpEnd));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    rawAuction.setQuantity(response.getJSONObject(count).getJSONObject("sell").getInt("quantity"));
                                    rawAuction.setBasePrice(response.getJSONObject(count).getDouble("basePrice"));

                                    sell.setAuction(rawAuction);

                                    sell.setProductId(response.getJSONObject(count).getJSONObject("sell").getInt("productId"));

                                    allAuctions.add(sell);
                                }
                                count ++;
                            }

                            aCallBack.onSuccess(allAuctions);

                        }catch (JSONException excep){
                            Log.i("parseAuctionErr", excep.getMessage().toString());
                        }
                    }
                }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
        });

        MySingleton.getInstance(context).addToRequestQueue(jsonArrayRequest);

    }





    public interface getAuctionCallBack{

        void onSucces(Auction auction);
        void onFail(String msg);
    }

    public interface getAllAuctionsCallBack{

        void onSuccess(ArrayList<Sell> rawAllCurrentAuctions);
    }

}
