package Tasks;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.fofofofodev.gbay.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import models.Auction;
import models.Category;
import models.Product;
import models.Purchase;
import models.Sell;
import models.Seller;

/**
 * Created by fofofofodev on 17/04/2017.
 */

public class SellerTask {

    private Context context;
    private Seller seller;
    private String rootReqUrl = "https://gbay.bmyguest.ovh/users/";

    private ArrayList<Purchase> purchases = new ArrayList<>();

    private ArrayList<Product> sellerProducts = new ArrayList<>();


    public SellerTask (Context aContext){
        context = aContext;
    }



    public void setProductsArray(JSONArray response){

        try{
            int count = 0;

            while(count < response.length()){
                JSONObject jsonObject = response.getJSONObject(count);

                Product product = new Product(jsonObject.getString("name").toString());
                product.setId(Integer.valueOf(jsonObject.getString("id")));

                product.setDescription(jsonObject.getString("description").toString());
                product.setPrice(Double.valueOf(jsonObject.getString("price").toString()));
                product.setDirectBuy(jsonObject.getBoolean("directBuy"));

                product.initProductSells(jsonObject.getJSONArray("sells"));

                product.initCatgeories(jsonObject.getJSONArray("categories"));

                product.initSeller(jsonObject.getJSONObject("seller"));
                if(!jsonObject.isNull("stock")){
                    product.setStock(jsonObject.getInt("stock"));
                }
                sellerProducts.add(product);
                count ++;
            }

        }catch (JSONException excep){
            Log.i("setProductsArray", excep.getMessage().toString());
        }
    }

    public void getProductsBySeller(String loggedUserId, final getSellerProductsCallback aCallback){

        String reqUrl = "https://gbay.bmyguest.ovh/products?sellerId=" + loggedUserId;

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(reqUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        sellerProducts.clear();
                        setProductsArray(response);
                        aCallback.onSuccess(sellerProducts);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "failed to connect to the API", Toast.LENGTH_LONG).show();
            }
        });

        MySingleton.getInstance(context).addToRequestQueue(jsonArrayRequest);
    }


    public void addCredits(int userId, String amount, final String token, final sellerTaskCallBack aCallBack){
        String reqUrl = rootReqUrl + String.valueOf(userId) + "/credits";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("credits", amount);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, reqUrl, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.i("addCredits", response.toString());
                         aCallBack.onSuccess(seller);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.i(getClass().getSimpleName(), error.getMessage().toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization",token);
                return headers;
            }
        };
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public void getSellerById(String sellerId, final String token, final sellerTaskCallBack aCallBack){
        String reqUrl = rootReqUrl + sellerId;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, reqUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        seller = new Seller();
                        seller.awakeUserFromJson(response);
                        aCallBack.onSuccess(seller);
                    }
                }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, "Echec connection pour getSellerById", Toast.LENGTH_LONG).show();
                }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization",token);
                return headers;
            }
        };
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public void addProduct(Product product, final String token, final addProductCallback aCallback){

        String reqUrl = "https://gbay.bmyguest.ovh/products";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", product.getName());
            jsonObject.put("description", product.getDescription());
            jsonObject.put("price", product.getPrice());
            jsonObject.put("stock", product.getStock());
            jsonObject.put("directBuy", String.valueOf(product.isDirectBuy()));
            //jsonObject.put("categoryId", product.getCategoryId());
            JSONArray categsArray = new JSONArray();
            for (Category category: product.getCategories()){
                categsArray.put(String.valueOf(category.getId()));
            }

            jsonObject.put("categories", categsArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("prodctToBeAdded", jsonObject.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, reqUrl, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("raw new Product", response.toString());
                        aCallback.onSucess();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Echec connection pour addProduct", Toast.LENGTH_LONG).show();
                    }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization",token);
                return headers;
            }

        };
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }


    public void getPurchases(final String token, String userId, final getPurchasesCallback aCallback){

        String req = rootReqUrl+userId+"/purchases";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, req, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                        int count = 0;
                                while(count < response.length()){
                                    Purchase purchase = new Purchase();
                                    JSONObject jsonObject = response.getJSONObject(count);

                                    if(!jsonObject.isNull("feedback")){
                                        purchase.setComment(jsonObject.getJSONObject("feedback").getString("comment"));
                                        purchase.setNote(jsonObject.getJSONObject("feedback").getDouble("note"));
                                    }
                                    if(!jsonObject.isNull("product")){
                                        purchase.setProductName(jsonObject.getJSONObject("product").getString("name"));
                                    }
                                    purchase.setQuantity(jsonObject.getInt("quantity"));
                                    purchase.setId(jsonObject.getInt("id"));

                                    purchases.add(purchase);
                                    count ++;
                                }

                            Log.i("Raw Purchases", response.toString());
                            aCallback.OnSuccess(purchases);
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "connect fail for getPurachases", Toast.LENGTH_LONG).show();
                    }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization",token);
                return headers;
            }
        };
        MySingleton.getInstance(context).addToRequestQueue(jsonArrayRequest);
    }


    public void updateFeedback(final String token, String sellId, Purchase purchase,final addProductCallback aCallback){

        String reqUrl = "https://gbay.bmyguest.ovh/sells/"+sellId+"/feedbacks";

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("note", String.valueOf(purchase.getNote()));
            jsonObject.put("comment", purchase.getComment());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, reqUrl, jsonObject,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.i("modified Feedback", response.toString());
                        aCallback.onSucess();
                    }
                }, new Response.ErrorListener() {
                   @Override
                   public void onErrorResponse(VolleyError error) {
                      // Log.i("fail PutFeedback", error.);
                    }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization",token);
                return headers;
            }

        };

        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }


    public  void addFeedBack(final String token, String sellId, Purchase purchase, final addProductCallback aCallback){

        String reqUrl = "https://gbay.bmyguest.ovh/sells/"+sellId+"/feedbacks";

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("note", String.valueOf(purchase.getNote()));
            jsonObject.put("comment", purchase.getComment());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, reqUrl, jsonObject,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        aCallback.onSucess();
                    }
                }, new Response.ErrorListener() {
                    @Override
                     public void onErrorResponse(VolleyError error) {
                        // Log.i("fail PutFeedback", error.);
                    }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization",token);
                return headers;
            }

        };

        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }


    public void deleteFeedBack(final String token, String sellId, final addProductCallback callback){

        String reqString = "https://gbay.bmyguest.ovh/sells/"+sellId+"/feedbacks";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, reqString, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("raw deleted", response.toString());
                        callback.onSucess();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("deletefeedback", error.getStackTrace().toString());
                //Toast.makeText(context,"delete feedBack failed", )
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization",token);
                return headers;
            }
        };
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }


    public Sell initSell(JSONObject jsonObject){
        Sell sell = null;
        Auction auction = null;
        SimpleDateFormat formatter =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        if(jsonObject != null){
            sell =  new Sell();
            auction = new Auction();
            try {
                auction.setId(Integer.valueOf(jsonObject.getString("id")));
                String tmpStart = jsonObject.getString("start");
                String tmpEnd = jsonObject.getString("end");
                auction.setStartDate(formatter.parse(tmpStart));
                auction.setEndDate(formatter.parse(tmpEnd));
                //auction.setQuantity(jsonObject.getInt("quantity"));
                auction.setBasePrice(jsonObject.getDouble("basePrice"));

                sell.setAuction(auction);


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return sell;
    }


    public void createAuction(final String token, final Auction auction, final createAuctionCallBack aCallback){

        JSONObject reqBody = new JSONObject();

        final String req = "https://gbay.bmyguest.ovh/auctions";

        try {
            reqBody.put("productId", auction.getProductId());
            reqBody.put("quantity", String.valueOf(auction.getQuantity()));
            reqBody.put("basePrice", String.valueOf(auction.getBasePrice()));
            reqBody.put("start", String.valueOf(auction.getStartDate()));
            reqBody.put("end", String.valueOf(auction.getEndDate()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest =  new JsonObjectRequest(Request.Method.POST, req, reqBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("raw new Auction", response.toString());
                        Sell sell = initSell(response);
                        sell.setQuantity(auction.getQuantity());
                        aCallback.onSuccess(sell);
                    }
                }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, "Echec Post Auction", Toast.LENGTH_LONG).show();
                    Log.i("CreateAuctionError", error.getMessage().toString());
                }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization",token);
                return headers;
            }
        };
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public interface getPurchasesCallback{
        void OnSuccess(ArrayList<Purchase> RawPurchases);
        void onFail(String msg);
    }

    public interface sellerTaskCallBack{

        void onSuccess(Seller seller);
        void onFail(String msg);
    }

    public interface addProductCallback{
         void onSucess();
        void onFail(String msg);
    }

    public interface getSellerProductsCallback{
        void onSuccess(ArrayList<Product> rawProducts);
    }

    public interface createAuctionCallBack{

        void onSuccess(Sell sell);
    }

}
