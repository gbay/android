package Tasks;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.fofofofodev.gbay.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import models.Auction;
import models.Category;
import models.Product;
import models.Sell;
import models.Seller;

/**
 * Created by fofofofodev on 29/03/2017.
 */

public class GetProductsTask {

    Context context;

    ArrayList<Product> productsArray = new ArrayList<>();
    String json_url = "https://gbay.bmyguest.ovh/products";
    //String json_url = "http://172.19.88.205:3000/products";

    ArrayList<Seller> sellersArray = new ArrayList<>();
    String getSellers_url = "https://gbay.bmyguest.ovh/sellers";
    //String getSellers_url = "http://172.19.88.205:3000/users";

    ArrayList<Category> categoriesArray =  new ArrayList<>();
    String getCatsUrl = "https://gbay.bmyguest.ovh/categories";
    //String getCatsUrl = "http://172.19.88.205:3000/categories";


    ArrayList<Auction> auctions = new ArrayList<>();
    String getAuctionsUrl = "https://gbay.bmyguest.ovh/auctions";
    //String getAuctionsUrl = "http://172.19.88.205:3000/auctions";

    //172.19.91.66

    public void getCategories(final getCategsCallBack categsCallBack)
    {

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(getCatsUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            int count = 0;
                            while (count < response.length()){
                                JSONObject jsonObject = response.getJSONObject(count);
                                Category category =  new Category();
                                category.setId(jsonObject.getInt("id"));
                                category.setName(jsonObject.getString("name"));
                                categoriesArray.add(category);
                                count ++;
                            }
                            categsCallBack.onSuccess(categoriesArray);

                        }catch (JSONException excep){
                            Toast.makeText(context,"Failed to parse categories", Toast.LENGTH_LONG).show();
                            excep.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context,"Failed to connect to API", Toast.LENGTH_LONG).show();
                    Log.i("failed to get response", error.getStackTrace().toString());
                }
        });

        MySingleton.getInstance(context).addToRequestQueue(jsonArrayRequest);
    }


    public  void getSellers(final getSellersCallBack arrayCallBack){

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(getSellers_url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try{

                            int count = 0;

                            while(count < response.length()){

                                JSONObject jsonObject = response.getJSONObject(count);
                                Seller  seller = new Seller();

                                seller.setId(jsonObject.getInt("id"));
                                seller.setPassword(jsonObject.getString("password"));
                                seller.setEmail(jsonObject.getString("email"));
                                seller.setName(jsonObject.getString("name"));
                                seller.setCredits(jsonObject.getDouble("credits"));
                                seller.setRoleId(jsonObject.getInt("roleId"));

                                sellersArray.add(seller);

                                count ++;
                            }

                            arrayCallBack.onSuccess(sellersArray);

                        }catch (JSONException e){

                            Toast.makeText(context,"Failed to parse sellers json", Toast.LENGTH_LONG).show();
                            Log.i("failed parse sellers", e.getStackTrace().toString());
                        }


                    }
                }, new Response.ErrorListener() {

                     @Override
                    public void onErrorResponse(VolleyError error) {

                         Toast.makeText(context,"Failed to connect to API", Toast.LENGTH_LONG).show();
                         Log.i("failed to get response", error.getStackTrace().toString());

                    }

        });

        MySingleton.getInstance(context).addToRequestQueue(jsonArrayRequest);

    }


    public Auction initAuction(JSONObject jsonObject){
        Auction auction = null;
        SimpleDateFormat formatter =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        if(jsonObject != null){
            auction = new Auction();
            try {
                auction.setId(Integer.valueOf(jsonObject.getString("id")));
                String tmpStart = jsonObject.getString("start");
                String tmpEnd = jsonObject.getString("end");
                auction.setStartDate(formatter.parse(tmpStart));
                auction.setEndDate(formatter.parse(tmpEnd));
                auction.setQuantity(jsonObject.getInt("quantity"));
                auction.setBasePrice(jsonObject.getDouble("basePrice"));
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return auction;
    }

    public  GetProductsTask(Context aContext){
         this.context = aContext;
    }

    public void setProductsArray(JSONArray response){

        try{
            int count = 0;

            while(count < response.length()){
                JSONObject jsonObject = response.getJSONObject(count);

                Product product = new Product(jsonObject.getString("name").toString());
                product.setId(Integer.valueOf(jsonObject.getString("id")));

                product.setDescription(jsonObject.getString("description").toString());
                product.setPrice(Double.valueOf(jsonObject.getString("price").toString()));
                product.setDirectBuy(jsonObject.getBoolean("directBuy"));

                product.initProductSells(jsonObject.getJSONArray("sells"));

                product.initCatgeories(jsonObject.getJSONArray("categories"));

                product.initSeller(jsonObject.getJSONObject("seller"));
                if(!jsonObject.isNull("stock")){
                    product.setStock(jsonObject.getInt("stock"));
                }
                productsArray.add(product);
                count ++;
            }

        }catch (JSONException excep){
            Log.i("setProductsArray", excep.getMessage().toString());
        }

    }

    public void getProducts(final arrayCallBack onSuccess){
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(json_url,
                new Response.Listener<JSONArray>(){
                    @Override
                    public void onResponse(JSONArray response) {
                        productsArray.clear();
                        setProductsArray(response);
                        onSuccess.onSuccess(productsArray);
                    }
                }, new Response.ErrorListener(){

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(context,"Failed to connect to API", Toast.LENGTH_LONG).show();
                            Log.i("failed to get response", error.getStackTrace().toString());
                        }
        });

        MySingleton.getInstance(context).addToRequestQueue(jsonArrayRequest);
    }

    public void getProductsBySeller(Seller seller, final arrayCallBack aCallback){

        String reqUrl = "https://gbay.bmyguest.ovh/products?sellerId=" + String.valueOf(seller.getId());

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(reqUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        productsArray.clear();
                        setProductsArray(response);
                        aCallback.onSuccess(productsArray);
                    }
                }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, "failed to connect to the API", Toast.LENGTH_LONG).show();
                }
        });

        MySingleton.getInstance(context).addToRequestQueue(jsonArrayRequest);
    }

    public void getProductByCateg(Category category, final getProductsByCategCallback aCallBack){

        String reqString = "https://gbay.bmyguest.ovh/products?categories="+String.valueOf(category.getId());

        JsonArrayRequest jsonArrayRequest =  new JsonArrayRequest(reqString,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        productsArray.clear();

                        setProductsArray(response);

                        aCallBack.onSuccess(productsArray);

                    }
                }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,"Erreur connection API", Toast.LENGTH_LONG).show();
                 }
        });

        MySingleton.getInstance(context).addToRequestQueue(jsonArrayRequest);
    }

    public void orderProductsByPrice( final arrayCallBack aCallback){

        String reqUrl = "https://gbay.bmyguest.ovh/products?order=price";

        JsonArrayRequest jsonArrayRequest =  new JsonArrayRequest(reqUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        productsArray.clear();
                        setProductsArray(response);
                        aCallback.onSuccess(productsArray);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Erreur connection API", Toast.LENGTH_LONG).show();
            }
        });

        MySingleton.getInstance(context).addToRequestQueue(jsonArrayRequest);
    }

    public interface getProductsByCategCallback{
        void onSuccess(ArrayList<Product> productsArray);
        void onFail(String msg);
    }


    public interface getCategsCallBack{
        void onSuccess(ArrayList<Category> categoriesArray);
        void onFail(String msg);
    }


    public interface getSellersCallBack{
        void onSuccess(ArrayList<Seller> sellersArray);
        void onFail(String msg);

    }

    public interface  arrayCallBack {
        void onSuccess(ArrayList<Product> products);
        void onFail(String msg);
    }

}
