package com.example.fofofofodev.gbay.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fofofofodev.gbay.R;

import java.util.ArrayList;

import models.Sell;

/**
 * Created by fofofofodev on 23/04/2017.
 */

public class SellerSellsAdapter extends RecyclerView.Adapter<SellerSellsAdapter.SellerSellsViewHolder> {

    ArrayList<Sell> sellerSells;

    public SellerSellsAdapter(ArrayList<Sell> someSells){sellerSells = someSells;}

    @Override
    public SellerSellsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.seller_sells_row_item, parent, false);
        SellerSellsViewHolder sellerSellsViewHolder = new SellerSellsViewHolder(view);
        return sellerSellsViewHolder;
    }

    @Override
    public void onBindViewHolder(SellerSellsViewHolder holder, int position) {
           // holder.buyerNameTextView.setText(sellerSells.get(position).getBuyer().getName());
            holder.sellQuantityTextView.setText(String.valueOf(sellerSells.get(position).getAuction().getQuantity()));
            holder.sellAmountTextView.setText(String.valueOf(sellerSells.get(position).getAuction().getBasePrice()));

    }

    @Override
    public int getItemCount() {
        return sellerSells.size();
    }


    public static class SellerSellsViewHolder extends RecyclerView.ViewHolder {
        TextView buyerNameTextView;
        TextView sellQuantityTextView;
        TextView sellAmountTextView;

        public SellerSellsViewHolder(View itemView) {
            super(itemView);
            buyerNameTextView =(TextView) itemView.findViewById(R.id.buyerNameTextView);
            sellQuantityTextView = (TextView) itemView.findViewById(R.id.sellQuantityTextView);
            sellAmountTextView = (TextView) itemView.findViewById(R.id.sellAmountTextView);

        }
    }


}
