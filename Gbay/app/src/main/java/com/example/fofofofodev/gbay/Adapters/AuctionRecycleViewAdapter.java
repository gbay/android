package com.example.fofofofodev.gbay.Adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.fofofofodev.gbay.AuctionDetailsActivity;
import com.example.fofofofodev.gbay.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import models.Sell;

/**
 * Created by fofofofodev on 10/04/2017.
 */

public class AuctionRecycleViewAdapter extends RecyclerView.Adapter<AuctionRecycleViewAdapter.ProductDetailViewHolder> {
    ArrayList<Sell> sells = new ArrayList<>();

    public AuctionRecycleViewAdapter(ArrayList<Sell> array){
        sells = array;
    }

    @Override
    public ProductDetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sell_row_item, parent, false);
        ProductDetailViewHolder pDVH = new ProductDetailViewHolder(view);
        return pDVH;
    }

    @Override
    public void onBindViewHolder(ProductDetailViewHolder holder, final int position) {
        holder.sellQuantityTV.setText(String.valueOf(sells.get(position).getQuantity()));

        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        holder.auctionStartTV.setText(formatter.format(sells.get(position).getAuction().getStartDate()));
        holder.auctionEndTV.setText(formatter.format(sells.get(position).getAuction().getEndDate()));

        holder.sellBasePriceTv.setText(String.valueOf(sells.get(position).getAuction().getBasePrice()));


        holder.seeAuctionDetailsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(v.getContext(), AuctionDetailsActivity.class);
                intent.putExtra("auctionId", String.valueOf(sells.get(position).getAuction().getId()));
                v.getContext().startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return sells.size();
    }


    public static class ProductDetailViewHolder extends RecyclerView.ViewHolder{

        TextView auctionStartTV;
        TextView auctionEndTV;
        TextView sellQuantityTV;
        TextView sellBasePriceTv;

        Button seeAuctionDetailsBtn;

        public ProductDetailViewHolder(View itemView) {
            super(itemView);
            auctionStartTV = (TextView) itemView.findViewById(R.id.auctionStartTV);
            auctionEndTV = (TextView) itemView.findViewById(R.id.auctionEndTV);
            sellQuantityTV = (TextView) itemView.findViewById(R.id.sellQuantityTV);
            seeAuctionDetailsBtn = (Button) itemView.findViewById(R.id.seeAuctionDetailsBtn);
            sellBasePriceTv = (TextView) itemView.findViewById(R.id.sellBasepriceTV);
        }

    }

}
