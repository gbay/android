package com.example.fofofofodev.gbay;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.example.fofofofodev.gbay.Adapters.SellerProductsAdapter;

import Tasks.GetProductsTask;
import Tasks.SellerTask;
import models.Category;
import models.Product;
import models.Seller;

public class SellerActivity extends AppCompatActivity {

    SellerTask sellerTask;

    private String loggedUserId;
    private String token;

    private Seller seller;

    TextView userCreditsTextView;
    Button addCredits;
    EditText addCreditsEditText;

    EditText productNameEditText;
    EditText productDescriptionEditText;
    EditText productStockEditText;
    Button addProductBtn;
    EditText productPriceEditText;
    EditText productDirectBuyEditText;
    EditText productCategoriesEditText;

    TextView defaultTextView;
    LinearLayout defaultTextLayout;
    LinearLayout sellerRVLayout;


    RecyclerView sellerRecycleView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter sellerProductsAdapter;

    ArrayList<Product> sellerProducts = new ArrayList<>();

    MenuInflater menuInflater;


    GetProductsTask getProductsTask;
    SharedPreferences.Editor preferencesEditor;
    SharedPreferences sharedPreferences;

    ArrayList<Category> categories = new ArrayList<>();
    ArrayList<Category> currentProductCategories = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller);
        manageVisibleLayouts();
        sharedPreferences = getApplication().getSharedPreferences(getString(R.string.sharedPreferencesFileName), Context.MODE_PRIVATE);
        preferencesEditor = sharedPreferences.edit();

        token = sharedPreferences.getString("token", null);
        loggedUserId = sharedPreferences.getString("userId", null);
        sellerTask = new SellerTask(this);
        getProductsTask = new GetProductsTask(this);

        userCreditsTextView = (TextView) findViewById(R.id.userCreditsTextView);
        addCredits = (Button) findViewById(R.id.addCredits);
        addCreditsEditText = (EditText) findViewById(R.id.addCreditsEditText);
        productCategoriesEditText = (EditText) findViewById(R.id.productCategoriesEditText);
        clickAddCredits();
        loadSeller();

        initViewELements();

        clickAddProduct();

        //this.setTitle(seller.getName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.homemenu, menu);
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(true);
        menu.getItem(2).setVisible(true);
        menu.getItem(4).setVisible(false);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Log.i("selectedItem", item.getTitle().toString());

        switch(item.getItemId()){

            case R.id.accountItem :

                break;

            case R.id.purchases:
                Intent intent = new Intent(this, sellerPurchasesActivity.class);
                intent.putExtra("token", token);
                intent.putExtra("userId", loggedUserId);
                startActivity(intent);


                break;
        }



        return super.onOptionsItemSelected(item);
    }

    private void initViewELements() {
        productNameEditText = (EditText) findViewById(R.id.productNameEditText);
        productDescriptionEditText = (EditText) findViewById(R.id.productDescriptionEditText);
        productStockEditText  =(EditText) findViewById(R.id.productStockEditText);
        addProductBtn = (Button) findViewById(R.id.addProductBtn);
        productPriceEditText = (EditText) findViewById(R.id.productPriceEditText);
        productDirectBuyEditText  =(EditText) findViewById(R.id.productDirectBuyEditText);
    }

    private void manageVisibleLayouts() {
        defaultTextView = (TextView) findViewById(R.id.defaultText);
        defaultTextLayout = (LinearLayout) findViewById(R.id.defaultTextLayout);
        sellerRVLayout = (LinearLayout) findViewById(R.id.sellerRVLayout);
        sellerRVLayout.setVisibility(View.INVISIBLE);
    }

    private void clickAddProduct() {
        addProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product product = new Product();
                product.setStock(Integer.valueOf(productStockEditText.getText().toString()));
                product.setDescription(productDescriptionEditText.getText().toString());
                product.setName(productNameEditText.getText().toString());
                product.setDirectBuy(Boolean.valueOf(productDirectBuyEditText.getText().toString()));
                product.setPrice(Double.valueOf(productPriceEditText.getText().toString()));
                CategoriesStringsToObjects(productCategoriesEditText.getText().toString().split(","));
                product.setCategories(currentProductCategories);
                sellerTask.addProduct(product, token, new SellerTask.addProductCallback() {
                    @Override
                    public void onSucess() {
                        sellerProducts.clear();
                        sellerProductsAdapter.notifyDataSetChanged();
                        loadSeller();

                        productDescriptionEditText.setText(null);
                        productDirectBuyEditText.setText(null);
                        productStockEditText.setText(null);
                        productNameEditText.setText(null);
                        productPriceEditText.setText(null);
                        productCategoriesEditText.setText(null);

                    }
                    @Override
                    public void onFail(String msg) {
                    }
                });

            }
        });


    }

    private void clickAddCredits() {
        addCredits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sellerTask.addCredits(Integer.valueOf(loggedUserId), addCreditsEditText.getText().toString(), token,
                        new SellerTask.sellerTaskCallBack() {
                            @Override
                            public void onSuccess(Seller seller) {
                                loadSeller();
                                addCreditsEditText.setText(null);
                            }
                            @Override
                            public void onFail(String msg) {

                            }
                        });
            }
        });
    }

    private void loadSeller() {
        sellerTask.getSellerById(loggedUserId, token, new SellerTask.sellerTaskCallBack() {
            @Override
            public void onSuccess(Seller rawsSeller) {
                seller = rawsSeller;
                loadProductSeller();
                userCreditsTextView.setText(String.valueOf(seller.getCredits()));
                if(seller.getUserProducts().size() > 0){
                    defaultTextLayout.setVisibility(View.INVISIBLE);
                    sellerRVLayout.setVisibility(View.VISIBLE);
                }
                getSupportActionBar().setTitle(seller.getName());

                preferencesEditor.putString("userName", seller.getName());
                preferencesEditor.commit();


            }
            @Override
            public void onFail(String msg) {}
        });
    }

    private void loadProductSeller(){
        sellerTask.getProductsBySeller(loggedUserId, new SellerTask.getSellerProductsCallback() {
            @Override
            public void onSuccess(ArrayList<Product> rawProducts) {
                sellerProducts.clear();
                sellerProductsAdapter.notifyDataSetChanged();
                sellerProducts.addAll(rawProducts);
                sellerProductsAdapter.notifyDataSetChanged();
            }
        });


    }
    @Override
    protected void onStart() {
        super.onStart();
        loadSeller();
        sellerRecycleView  = (RecyclerView) findViewById(R.id.sellerRecyclerView);
        layoutManager =  new LinearLayoutManager(this);
        sellerProductsAdapter =  new SellerProductsAdapter(sellerProducts);
        sellerRecycleView.setLayoutManager(layoutManager);
        sellerRecycleView.setAdapter(sellerProductsAdapter);
        getProductsTask.getCategories(new GetProductsTask.getCategsCallBack() {
            @Override
            public void onSuccess(ArrayList<Category> categoriesArray) {
                categories.addAll(categoriesArray);
            }

            @Override
            public void onFail(String msg) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void CategoriesStringsToObjects(String[] categStrings){
        for(int i = 0; i <categStrings.length; i++){
            for(Category category : categories){
                if(categStrings[i].equals(category.getName())){
                    currentProductCategories.add(category);
                }
            }
        }
    }


}
