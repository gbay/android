package com.example.fofofofodev.gbay;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import Tasks.GetProductsTask;
import Tasks.ProductTask;
import models.Category;
import models.Product;
import models.Sell;

public class EditProductActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    EditText editNameTv;
    EditText editDescriptionTv;
    EditText editDirectBuyTV;
    EditText editStockTv;
    EditText editPriceTv;
    EditText productCategoriesEditText;

    Button editProductBtn;
    Button deleteProductBtn;

    String productId;

    Product product;

    ProductTask productTask;
    String token;


    ArrayList<Category> categories = new ArrayList<>();

    ArrayList<Category> productCategories = new ArrayList<>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);

        editNameTv = (EditText) findViewById(R.id.editNameTv);
        editDescriptionTv = (EditText) findViewById(R.id.editDescriptionTv);
        editDirectBuyTV = (EditText) findViewById(R.id.editDirectBuyTV);
        editStockTv = (EditText) findViewById(R.id.editStockTv);
        editPriceTv = (EditText) findViewById(R.id.editPriceTv);
        editProductBtn = (Button) findViewById(R.id.editProductBtn);
        deleteProductBtn = (Button) findViewById(R.id.deleteProductBtn);
        productCategoriesEditText = (EditText) findViewById(R.id.productCategoriesEditText);



        SharedPreferences sharedPreferences = getApplication().getSharedPreferences(getString(R.string.sharedPreferencesFileName), Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", null);
        productId = getIntent().getStringExtra("productId");

        getSupportActionBar().setTitle(sharedPreferences.getString("userName", null));

        productTask = new ProductTask(this);
        loadProduct();
        touchEditProduct();

        deleteProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productTask.deleteProduct(token, productId, new ProductTask.updateProductCallback() {
                    @Override
                    public void onSucces() {
                        startActivity(new Intent(getApplicationContext(),SellerActivity.class));
                    }
                    @Override
                    public void onFail() {}
                });
            }
        });

    }

    private void touchEditProduct() {
        editProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                product = new Product();
                product.setId(Integer.valueOf(productId));
                product.setDescription(editDescriptionTv.getText().toString());
                product.setStock(Integer.valueOf(editStockTv.getText().toString()));
                product.setName(editNameTv.getText().toString());
                product.setPrice(Double.valueOf(editPriceTv.getText().toString()));
                product.setDirectBuy(Boolean.valueOf(editDirectBuyTV.getText().toString()));
                CategoriesStringsToObjects(productCategoriesEditText.getText().toString().split(","));
                product.setCategories(productCategories);

                productTask.updateProduct(token, product, new ProductTask.getProductByIdCallBack() {
                    @Override
                    public void onSuccess(Product product) {
                        startActivity(new Intent(getApplicationContext(), SellerActivity.class));
                        loadProduct();
                    }
                    @Override
                    public void onFail(String msg) {}
                });

            }
        });
    }

    private void loadProduct() {
        productTask.getProductById(productId, new ProductTask.getProductByIdCallBack() {
            @Override
            public void onSuccess(Product rawProduct) {
                product = rawProduct;
                productCategories.addAll(rawProduct.getCategories());

                editNameTv.setText(product.getName());
                editDescriptionTv.setText(product.getDescription());
                editDirectBuyTV.setText(String.valueOf(product.isDirectBuy()));
                editStockTv.setText(String.valueOf(product.getStock()));
                editPriceTv.setText(String.valueOf(product.getPrice()));
                productCategoriesEditText.setText(product.CategsToString());

            }
            @Override
            public void onFail(String msg) {}
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {}

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}


    @Override
    protected void onStart() {
        super.onStart();

        GetProductsTask getProductsTask = new GetProductsTask(this);

        getProductsTask.getCategories(new GetProductsTask.getCategsCallBack() {
            @Override
            public void onSuccess(ArrayList<Category> categoriesArray) {
                categories.addAll(categoriesArray);
            }
            @Override
            public void onFail(String msg) {}
        });

    }

    public String [] categsArrayToStringArray(){
        String [] res = new String [categories.size()];
        for(int i = 0; i < categories.size();i++){
            res[i] = categories.get(i).getName();
        }
        return res;
    }

    public void CategoriesStringsToObjects(String[] categStrings){
        for(int i = 0; i <categStrings.length; i++){
            for(Category category : categories){
                if(categStrings[i].equals(category.getName())){
                    if(!productCategories.isEmpty()){
                        for(Category productCategs : productCategories){
                            if(!productCategs.getName().equals(category.getName())){
                                productCategories.add(category);
                            }
                        }
                    }
                    else{
                        productCategories.add(category);
                    }
                }
            }
        }
    }
}
