package com.example.fofofofodev.gbay;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fofofofodev.gbay.Adapters.AuctionRecycleViewAdapter;
import com.example.fofofofodev.gbay.Adapters.SellerSellsAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import Tasks.ProductTask;
import Tasks.SellerTask;
import models.Auction;
import models.Product;
import models.Sell;

public class manageAuctionsActivity extends AppCompatActivity {

    String productId;

    ProductTask productTask;


    ArrayList<Sell> productAuctions = new ArrayList<>();

    TextView auctionsDefaultText;

    RecyclerView sellerAuctionsRecyclerView;
    RecyclerView.Adapter sellerAuctionsRVAdapter;
    RecyclerView.LayoutManager sellerAuctionsLayoutManager;


    int StartYear, StartMonth, startDay, endYear, endMonth, endDay;

    Button chooseStartDate;
    TextView auctionStartDate;
    TextView endDateTextView;
    Button chooseEndDate;

    EditText auctionQuantityTextView;
    EditText auctionABasePriceTextView;
    Button createAuctionBtn;

    SimpleDateFormat simpleDateFormat;

    SellerTask sellerTask;

    String token;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_auctions);


        auctionsDefaultText = (TextView) findViewById(R.id.auctionsDefaultText);
        token = getSharedPreferences(getString(R.string.sharedPreferencesFileName), Context.MODE_PRIVATE).getString("token", null);
        getSupportActionBar().setTitle(getSharedPreferences(getString(R.string.sharedPreferencesFileName), Context.MODE_PRIVATE).getString("userName", null));

        initRecyclerView();

        sellerTask =  new SellerTask(this);
        productTask = new ProductTask(this);
        productId = getIntent().getStringExtra("productId");

        loadProduct();

        initDateTextViews();

        chooseStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            @SuppressWarnings("deprecation")
            public void onClick(View v) {
                showDialog(999);}
        });
        chooseEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            @SuppressWarnings("deprecation")
            public void onClick(View v) {
                showDialog(1000);
            }
        });

        createAuctionBtn = (Button) findViewById(R.id.createAuctionBtn);
        auctionABasePriceTextView = (EditText) findViewById(R.id.auctionBasePriceEditText);
        auctionQuantityTextView = (EditText) findViewById(R.id.auctionQuantityEditText);


        createAuctionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Auction auction = new Auction();
                simpleDateFormat =  new SimpleDateFormat("dd-MM-yyyy");
                try {
                    auction.setStartDate(simpleDateFormat.parse(auctionStartDate.getText().toString()));
                    auction.setEndDate(simpleDateFormat.parse(endDateTextView.getText().toString()));
                    auction.setProductId(Integer.valueOf(productId));
                    auction.setBasePrice(Double.valueOf(auctionABasePriceTextView.getText().toString()));
                    auction.setQuantity(Integer.valueOf(auctionQuantityTextView.getText().toString()));
                } catch (ParseException e) {
                    Log.i("auctionDate", e.getMessage().toString());
                    e.printStackTrace();
                }
                sellerTask.createAuction(token, auction, new SellerTask.createAuctionCallBack() {
                    @Override
                    public void onSuccess(Sell sell) {
                        productAuctions.add(sell);
                        sellerAuctionsRVAdapter.notifyDataSetChanged();
                    }
                });

                auctionQuantityTextView.setText(null);
                auctionABasePriceTextView.setText(null);

                endDateTextView.clearComposingText();
                auctionStartDate.clearComposingText();
            }
        });

    }

    private void loadProduct() {
        productTask.getProductById(productId, new ProductTask.getProductByIdCallBack() {
            @Override
            public void onSuccess(Product product) {
                if(!product.getProductSellsWithCurrentAuctions().isEmpty()){
                    auctionsDefaultText.setVisibility(View.INVISIBLE);
                }

                productAuctions.clear();
                sellerAuctionsRVAdapter.notifyDataSetChanged();
                productAuctions.addAll(product.getProductSellsWithCurrentAuctions());
                sellerAuctionsRVAdapter.notifyDataSetChanged();
            }
            @Override
            public void onFail(String msg) {}
        });
    }

    private void initRecyclerView() {
        sellerAuctionsRecyclerView = (RecyclerView) findViewById(R.id.SellerAuctionsRecyclerView);
        sellerAuctionsRVAdapter = new AuctionRecycleViewAdapter(productAuctions);
        sellerAuctionsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,true);
        sellerAuctionsRecyclerView.setLayoutManager(sellerAuctionsLayoutManager);
        sellerAuctionsRecyclerView.setAdapter(sellerAuctionsRVAdapter);
    }

    private void initDateTextViews() {
        chooseStartDate = (Button) findViewById(R.id.chooseStartDate);
        auctionStartDate = (TextView) findViewById(R.id.startDateTextView);

        chooseEndDate = (Button) findViewById(R.id.chooseEndDate);
        endDateTextView = (TextView) findViewById(R.id.endDateTextView);

        Calendar calendar = Calendar.getInstance(Locale.FRANCE);
        StartYear = calendar.get(calendar.YEAR);
        endYear = calendar.get(calendar.YEAR);
        StartMonth = calendar.get(calendar.MONTH);
        endMonth = calendar.get(calendar.MONTH);
        startDay = calendar.get(calendar.DAY_OF_MONTH);
        endDay =calendar.get(calendar.DAY_OF_MONTH);

        setTextView(auctionStartDate, StartYear, StartMonth, startDay);
        setTextView(endDateTextView,endYear,endMonth,endDay);
    }

    @SuppressWarnings("deprecation")
    protected Dialog onCreateDialog(int id) {
        if(id == 999){
            return  new DatePickerDialog(this, pickADateListener,StartYear,StartMonth,startDay);
        }
        if(id == 1000){
            return  new DatePickerDialog(this, pickAnEndDateListener,endYear,endMonth,endDay);
        }
        return null;
    }

    DatePickerDialog.OnDateSetListener pickADateListener =
            new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            setTextView(auctionStartDate, year, month, dayOfMonth);
        }
    };

    DatePickerDialog.OnDateSetListener pickAnEndDateListener =
            new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    setTextView(endDateTextView,year,month,dayOfMonth);
                }
            };

    public void setTextView(TextView texview, int year, int mounth, int day){
        String monthString = ((mounth + 1) < 10)? "0" + String.valueOf((mounth + 1)) : String.valueOf((mounth + 1));
        String dayString =  (day < 10) ? "0" + String.valueOf((day)) : String.valueOf((day));
        texview.setText( dayString + "-" +  monthString + "-" + String.valueOf(year));
    }

}
