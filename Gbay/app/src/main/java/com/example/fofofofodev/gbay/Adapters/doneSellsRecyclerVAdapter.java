package com.example.fofofofodev.gbay.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fofofofodev.gbay.R;

import java.util.ArrayList;

import models.Bid;

/**
 * Created by fofofofodev on 14/04/2017.
 */

public class doneSellsRecyclerVAdapter extends RecyclerView.Adapter<doneSellsRecyclerVAdapter.DoneSellsViewHolder> {

    ArrayList<Bid> auctionBids;

    public doneSellsRecyclerVAdapter (ArrayList<Bid> givenSells){
        auctionBids = givenSells;
    }

    @Override
    public DoneSellsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.done_sells_row_item, parent, false);

        DoneSellsViewHolder myViewHolder = new DoneSellsViewHolder(v);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final DoneSellsViewHolder holder, int position) {
        holder.bidOrSellAmountTV.setText(String.valueOf(auctionBids.get(position).getAmount()));
        holder.bidderOrBuyerNameTV.setText(auctionBids.get(position).getBidder().getName());
    }

    @Override
    public int getItemCount() {
        return auctionBids.size();
    }


    public static class DoneSellsViewHolder extends RecyclerView.ViewHolder{

        TextView bidderOrBuyerNameTV;
        TextView bidOrSellAmountTV;


        public DoneSellsViewHolder(View itemView){
            super(itemView);
            bidderOrBuyerNameTV = (TextView) itemView.findViewById(R.id.buyerNameTV);
            bidOrSellAmountTV = (TextView) itemView.findViewById(R.id.bidOrBuyAmountTV);
        }


    }


}
