package com.example.fofofofodev.gbay;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.example.fofofofodev.gbay.Adapters.AuctionRecycleViewAdapter;

import Tasks.AuctionTask;
import Tasks.ProductTask;
import models.Auction;
import models.Product;
import models.Sell;

public class ProductDetailsActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    ProductTask productTask;

    Product product;


    TextView productNameTV;
    TextView productPriceTV;
    TextView sellerNameTV;
    TextView categoriesTV;
    TextView descriptionTV;
    TextView productStockTV;
    EditText quantityToDirectBuyEditText;
    TextView defaultTextTV;
    TextView currentAuctionsLayoutHeader;

    LinearLayout directBuyBtnLayout;
    LinearLayout currentAuctionsLayout;

    Button directBuyBtn;
    AuctionTask auctionTask;


    RecyclerView auctionsBidsRV;
    RecyclerView.Adapter auctionsBidsRVAdapter;
    RecyclerView.LayoutManager layoutManager;


    private ArrayList<Sell> productSellsWithCurrentAuctions = new ArrayList<>();
    private ArrayList<Auction> allAuctions = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        sharedPreferences = getApplication().getSharedPreferences(getString(R.string.sharedPreferencesFileName), Context.MODE_PRIVATE);
        getSupportActionBar().setTitle(getSharedPreferences(getString(R.string.sharedPreferencesFileName), Context.MODE_PRIVATE).getString("userName", null));

        productTask =  new ProductTask(this);
        directBuyBtn = (Button) findViewById(R.id.directBuyButton);
        currentAuctionsLayout = (LinearLayout) findViewById(R.id.currentAuctionsLayout);
        directBuyBtnLayout = (LinearLayout) findViewById(R.id.directBuyButtonLayout);
        defaultTextTV = (TextView) findViewById(R.id.defaultTextTV);
        currentAuctionsLayoutHeader = (TextView) findViewById(R.id.currentAuctionsLayoutHeader);

        Intent intent = getIntent();
        productTask.getProductById(intent.getStringExtra("productId"), new ProductTask.getProductByIdCallBack() {

            @Override
            public void onSuccess(Product RawProduct) {
                product = RawProduct;

                if (product.isDirectBuy()) {
                    currentAuctionsLayout.setVisibility(View.INVISIBLE);
                    currentAuctionsLayout.setEnabled(false);
                } else {
                    directBuyBtnLayout.setVisibility(View.INVISIBLE);
                    directBuyBtnLayout.setEnabled(false);
                    directBuyBtn.setVisibility(View.INVISIBLE);
                }


                productNameTV = (TextView) findViewById(R.id.productNameTV);
                productPriceTV = (TextView) findViewById(R.id.productPriceTV);
                sellerNameTV = (TextView) findViewById(R.id.sellerNameTV);
                categoriesTV = (TextView) findViewById(R.id.categoriesTV);
                descriptionTV = (TextView) findViewById(R.id.descriptionTV);
                productStockTV = (TextView) findViewById(R.id.stockTextView);

                productNameTV.setText(product.getName());
                productPriceTV.setText(String.valueOf(product.getPrice()));
                sellerNameTV.setText(product.getSeller().getName());
                categoriesTV.setText(product.CategsToString());
                descriptionTV.setText(product.getDescription());
                productStockTV.setText(String.valueOf(product.getStock()));

                if(!product.getProductSellsWithCurrentAuctions().isEmpty()){
                    defaultTextTV.setVisibility(View.INVISIBLE);
                }
                else{
                    currentAuctionsLayoutHeader.setVisibility(View.INVISIBLE);
                }

                Log.i("auctions", product.getProductSellsWithCurrentAuctions().toString());

                productSellsWithCurrentAuctions.addAll(product.getProductSellsWithCurrentAuctions());
                auctionsBidsRVAdapter.notifyDataSetChanged();

            }
            @Override
            public void onFail(String msg) {}
        });

        quantityToDirectBuyEditText = (EditText) findViewById(R.id.directBuyQuantityEditTex);
        clickDirectBuy();

        auctionTask = new AuctionTask(this);






    }

    private void clickDirectBuy() {
        directBuyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sharedPreferences.getString("token", "null");

                Sell sell = new Sell();
                sell.setProductId(product.getId());
                sell.setQuantity(Integer.valueOf(quantityToDirectBuyEditText.getText().toString()));

                productTask.buyProduct(sell, sharedPreferences.getString("token", "null"), new ProductTask.getProductByIdCallBack() {
                    @Override
                    public void onSuccess(Product rawProduct) {
                        finish();
                        startActivity(getIntent());
                        product = null;
                        product = rawProduct;
                    }

                    @Override
                    public void onFail(String msg) {

                    }
                });

            }
        });
    }

    @Override
    protected void onResume() {super.onResume();}

    @Override
    protected void onStart() {
        super.onStart();

        auctionsBidsRV = (RecyclerView) findViewById(R.id.auctionsBidsRecyclerView);
        layoutManager = new LinearLayoutManager(this);

        auctionsBidsRV.setLayoutManager(layoutManager);
        auctionsBidsRVAdapter = new AuctionRecycleViewAdapter(productSellsWithCurrentAuctions);
        auctionsBidsRV.setAdapter(auctionsBidsRVAdapter);
        auctionsBidsRV.setHasFixedSize(true);
    }




}
