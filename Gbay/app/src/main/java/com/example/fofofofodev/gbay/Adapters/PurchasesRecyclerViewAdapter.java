package com.example.fofofofodev.gbay.Adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.fofofofodev.gbay.R;

import java.util.ArrayList;

import models.Purchase;

import com.example.fofofofodev.gbay.sellerFeedBacksActivity;

/**
 * Created by fofofofodev on 19/04/2017.
 */

public class PurchasesRecyclerViewAdapter extends RecyclerView.Adapter<PurchasesRecyclerViewAdapter.purchasesViewHolder>{

    ArrayList<Purchase> purchases = new ArrayList<>();

    public PurchasesRecyclerViewAdapter(ArrayList<Purchase> somePurchases){
        purchases = somePurchases;
    }


    @Override
    public purchasesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.purchase_row_layout, parent, false);
        purchasesViewHolder purchaseVH = new purchasesViewHolder(view);
        return purchaseVH;
    }

    @Override
    public void onBindViewHolder(final purchasesViewHolder holder, final int position) {
        holder.purchaseNote.setText(String.valueOf(purchases.get(position).getNote()));
        holder.purchaseName.setText(purchases.get(position).getProductName());
        holder.purchaseQuantity.setText(String.valueOf(purchases.get(position).getQuantity()));
        holder.purchaseComment.setText(purchases.get(position).getComment());

        if(purchases.get(position).getComment() == null  || purchases.get(position).getNote() == 0){
            holder.EditFeedBackBtn.setText("AJOUTER UN FEEDBACK");
        }

        holder.EditFeedBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), sellerFeedBacksActivity.class);
                intent.putExtra("sellId", String.valueOf(purchases.get(position).getId()));
                intent.putExtra("buttonValue", holder.EditFeedBackBtn.getText().toString());
                intent.putExtra("note", String.valueOf(holder.purchaseNote.getText().toString()));
                intent.putExtra("comment", String.valueOf(holder.purchaseComment.getText().toString()));
                v.getContext().startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return purchases.size();
    }

    public static class purchasesViewHolder extends RecyclerView.ViewHolder{

        TextView purchaseName;
        TextView purchaseQuantity;
        TextView purchaseNote;
        TextView purchaseComment;

        Button EditFeedBackBtn;

        public purchasesViewHolder(View itemView) {
            super(itemView);

            purchaseName = (TextView) itemView.findViewById(R.id.purchaseProductName);
            purchaseQuantity =(TextView) itemView.findViewById(R.id.purchaseQuantity);
            purchaseNote = (TextView) itemView.findViewById(R.id.purchaseNote);
            EditFeedBackBtn = (Button) itemView.findViewById(R.id.editFeedBackBtn);
            purchaseComment = (TextView) itemView.findViewById(R.id.purchaseComment);
        }
    }

}
