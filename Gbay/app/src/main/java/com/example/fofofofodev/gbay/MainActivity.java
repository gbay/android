package com.example.fofofofodev.gbay;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import com.example.fofofofodev.gbay.Adapters.RecyclerAdapter;
import Tasks.GetProductsTask;
import models.Category;
import models.Product;
import models.Seller;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor preferencesEditor;
    GetProductsTask getProductsTask;
    MenuInflater menuInflater;


    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    ArrayList<Product> productsArray = new ArrayList<>();

    ArrayList<Seller> sellers = new ArrayList<>();

    ArrayList<Category> categories = new ArrayList<>();


    Spinner firstSpinner;
    Spinner secondSpiner;
    ArrayAdapter<String> spinner2Adapter = null;


    Button filterByButton;
    Button orderByPriceBtn;


    Intent connectIntent;
    String token;
    String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getProductsTask = new GetProductsTask(MainActivity.this);

        getProductsTask.getProducts(new GetProductsTask.arrayCallBack() {
            @Override
            public void onSuccess(ArrayList<Product> products) {
                productsArray.addAll(products);
                adapter.notifyDataSetChanged();

                productsArrayToCategsArray();
            }

            @Override
            public void onFail(String msg) {

            }
        });

        initSpinners();

        touchFilter();

        touchOrderByPrice();

        sharedPreferences = getApplication().getSharedPreferences(getString(R.string.sharedPreferencesFileName), Context.MODE_PRIVATE);
        preferencesEditor = sharedPreferences.edit();
       // getSupportActionBar().setTitle(getSharedPreferences(getString(R.string.sharedPreferencesFileName), Context.MODE_PRIVATE).getString("userName", null));

    }

    private void initSpinners() {
        firstSpinner = (Spinner) findViewById(R.id.spinner_1);
        ArrayAdapter<CharSequence> spinner1Adapter = ArrayAdapter.createFromResource(this,R.array.spiner1_resources, android.R.layout.simple_spinner_item);
        spinner1Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        firstSpinner.setAdapter(spinner1Adapter);
        firstSpinner.setOnItemSelectedListener(this);
        secondSpiner = (Spinner) findViewById(R.id.spinner_2);
        filterByButton = (Button) findViewById(R.id.filterByButton);
    }

    private void touchOrderByPrice() {
        orderByPriceBtn = (Button) findViewById(R.id.orderByPriceBtn);

        orderByPriceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getProductsTask.orderProductsByPrice(new GetProductsTask.arrayCallBack() {
                    @Override
                    public void onSuccess(ArrayList<Product> products) {
                        productsArray.clear();
                        adapter.notifyDataSetChanged();
                        productsArray.addAll(products);
                        adapter.notifyDataSetChanged();
                    }
                    @Override
                    public void onFail(String msg) {

                    }
                });
            }
        });
    }

    private void touchFilter() {
        filterByButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(firstSpinner.getSelectedItem().toString()){
                    case "Catégorie":
                        Category queriedCat = StringToCateg(secondSpiner.getSelectedItem().toString());
                        getProductsTask.getProductByCateg(queriedCat, new GetProductsTask.getProductsByCategCallback() {
                            @Override
                            public void onSuccess(ArrayList<Product> rawProductsArray) {
                                productsArray.clear();
                                adapter.notifyDataSetChanged();
                                productsArray.addAll(rawProductsArray);
                                //bindAuctionsWithProducts();
                                adapter.notifyDataSetChanged();
                            }
                            @Override
                            public void onFail(String msg) {
                            }
                        });
                        break;

                    case "Vendeur":
                        Seller queriedSeller = StringToSeller(secondSpiner.getSelectedItem().toString());
                        getProductsTask.getProductsBySeller(queriedSeller, new GetProductsTask.arrayCallBack(){
                            @Override
                            public void onSuccess(ArrayList<Product> products) {
                                productsArray.clear();
                                adapter.notifyDataSetChanged();
                                productsArray.addAll(products);
                                adapter.notifyDataSetChanged();
                            }
                            @Override
                            public void onFail(String msg) {}
                        });
                        break;
                    default:
                        Toast.makeText(MainActivity.this,"Choisissez un filtre", Toast.LENGTH_LONG).show();
                        break;
                }

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.homemenu,menu);

        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.connectItem:
                connectIntent = new Intent(MainActivity.this, MyLoginActivity.class);
                startActivityForResult(connectIntent, 1);
                break;
            case R.id.accountItem:
                if(token == null){ Toast.makeText(getApplicationContext(),"Connectez vous ou inscrivez vous avant", Toast.LENGTH_LONG).show();}
                else{
                    Intent accountIntent = new Intent(MainActivity.this, SellerActivity.class);
                    accountIntent.putExtra("loggedUserId", userId);
                    accountIntent.putExtra("token", token);
                    startActivity(accountIntent);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                token = data.getStringExtra("token");
                userId = data.getStringExtra("loggedUserId");
                preferencesEditor.putString("token", token);
                preferencesEditor.putString("userId", data.getStringExtra("loggedUserId"));
                preferencesEditor.commit();
                getSupportActionBar().setTitle(getSharedPreferences(getString(R.string.sharedPreferencesFileName), Context.MODE_PRIVATE).getString("userName", null));

            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        getProductsTask.getSellers(new GetProductsTask.getSellersCallBack(){
            @Override
            public void onSuccess(ArrayList<Seller> sellersArray) {
                sellers.addAll(sellersArray);
            }
            @Override
            public void onFail(String msg) {

            }
        });
    }


    public void productsArrayToCategsArray(){
        for(int i = 0; i < productsArray.size(); i++){
            Product current = productsArray.get(i);
            for(int j = 0; j < current.getCategories().size();j++){

                if(!categories.contains(current.getCategories().get(j)))
                {
                    categories.add(current.getCategories().get(j));
                }
            }
        }
    }


    public String [] categsArrayToStringArray(){
        String [] res = new String [categories.size()];
        for(int i = 0; i < categories.size();i++){
            res[i] = categories.get(i).getName();
        }
        return res;
    }

    public String [] sellersArrayToStringArray(){
        String [] result = new String[sellers.size()];
        for(int i = 0; i < sellers.size(); i++){
            result[i] = sellers.get(i).getName();
        }
        return result;
    }

    public Category StringToCateg(String categName){
        Category result = null;
        for(int i = 0; i <categories.size(); i++){
            if(categories.get(i).getName() == categName){
                result = categories.get(i);
            }
        }
        return result;
    }

    public Seller StringToSeller(String sellerName){
        Seller result = null;

        for(int i = 0; i <sellers.size(); i++){
            if(sellers.get(i).getName() == sellerName){
                result = sellers.get(i);
            }
        }

        return result;
    }


    @Override
    protected void onStart() {
        super.onStart();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        adapter = new RecyclerAdapter(productsArray);
        recyclerView.setAdapter(adapter);

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case 0 :
                break;
            case 1 :
                if(spinner2Adapter != null){spinner2Adapter.clear();}
                spinner2Adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,categsArrayToStringArray());
                spinner2Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                secondSpiner.setAdapter(spinner2Adapter);
                break;
            case 2 :
                if(spinner2Adapter != null){spinner2Adapter.clear();}
                spinner2Adapter =  new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sellersArrayToStringArray());
                spinner2Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                secondSpiner.setAdapter(spinner2Adapter);
                break;
            default:
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}

}
