package com.example.fofofofodev.gbay.Adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.fofofofodev.gbay.EditProductActivity;
import com.example.fofofofodev.gbay.manageAuctionsActivity;
import com.example.fofofofodev.gbay.R;

import java.util.ArrayList;

import models.Product;

/**
 * Created by fofofofodev on 17/04/2017.
 */

public class SellerProductsAdapter extends RecyclerView.Adapter<SellerProductsAdapter.sellerProductsViewHolder> {

    private ArrayList<Product> sellerProducts = new ArrayList<>();

    public SellerProductsAdapter(ArrayList<Product> products){
        sellerProducts = products;
    }


    @Override
    public sellerProductsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.seller_products_row_item, parent, false);
        sellerProductsViewHolder myViewHolder = new sellerProductsViewHolder(v);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(sellerProductsViewHolder holder, final int position) {

        holder.sellerProductDescriptionTV.setText(sellerProducts.get(position).getDescription());
        holder.sellerProductNameTV.setText(sellerProducts.get(position).getName());
        holder.sellerProductPriceTV.setText(String.valueOf(sellerProducts.get(position).getPrice()));
        holder.sellerProductStockTV.setText(String.valueOf(sellerProducts.get(position).getStock()));
        holder.sellerProductDirectBuy.setText(String.valueOf(sellerProducts.get(position).isDirectBuy()));

        if(sellerProducts.get(position).isDirectBuy()){holder.goToSellerSells.setVisibility(View.INVISIBLE);}

        holder.editSellerProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), EditProductActivity.class);
                intent.putExtra("productId", String.valueOf(sellerProducts.get(position).getId()));
                intent.putExtra("productPrice", String.valueOf(sellerProducts.get(position).getPrice()));
                v.getContext().startActivity(intent);
            }
        });

        holder.goToSellerSells.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), manageAuctionsActivity.class);
                intent.putExtra("productId", String.valueOf(sellerProducts.get(position).getId()));
                v.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return sellerProducts.size();
    }

    public static class sellerProductsViewHolder extends RecyclerView.ViewHolder{
        TextView sellerProductNameTV;
        TextView sellerProductStockTV;
        TextView sellerProductPriceTV;
        TextView sellerProductDescriptionTV;
        TextView sellerProductDirectBuy;

        Button editSellerProduct;

        Button goToSellerSells;


        public sellerProductsViewHolder(View itemView) {
            super(itemView);

            sellerProductNameTV = (TextView) itemView.findViewById(R.id.sellerProductNameTV);
            sellerProductStockTV =(TextView) itemView.findViewById(R.id.sellerProductStockTV);
            sellerProductPriceTV = (TextView) itemView.findViewById(R.id.sellerProductPriceTV);
            sellerProductDescriptionTV = (TextView) itemView.findViewById(R.id.sellerProductDescriptionTV);
            sellerProductDirectBuy = (TextView) itemView.findViewById(R.id.sellerProductDirectBuy);



            goToSellerSells = (Button) itemView.findViewById(R.id.goToSellerSells);

            editSellerProduct = (Button) itemView.findViewById(R.id.editSellerProduct);

        }
    }


}
