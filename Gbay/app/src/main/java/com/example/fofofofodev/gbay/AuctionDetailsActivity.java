package com.example.fofofofodev.gbay;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import com.example.fofofofodev.gbay.Adapters.doneSellsRecyclerVAdapter;
import Tasks.AuctionTask;
import models.Auction;
import models.Bid;

public class AuctionDetailsActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    private AuctionTask auctionTask;
    private Auction auction;
    private int auctionId;

    RecyclerView bidsRecyclerView;
    RecyclerView.Adapter bidRVAdapter;
    RecyclerView.LayoutManager layoutManager;


    private ArrayList<Bid> auctionBids = new ArrayList<>();

    Button makeABidBtn;
    EditText bidAmountEditText;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auction_details);

        Intent intent =  getIntent();
        auctionId = Integer.valueOf(intent.getStringExtra("auctionId"));

        auctionTask = new AuctionTask(this);

        loadAuction();

        sharedPreferences = getApplication().getSharedPreferences(getString(R.string.sharedPreferencesFileName), Context.MODE_PRIVATE);
        getSupportActionBar().setTitle(getSharedPreferences(getString(R.string.sharedPreferencesFileName), Context.MODE_PRIVATE).getString("userName", null));

        bidAmountEditText = (EditText) findViewById(R.id.bidAmountEditText);
        makeABidBtn = (Button) findViewById(R.id.makeABidBtn);

        makeABidBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auctionTask.makeABid(auction, bidAmountEditText.getText().toString(), sharedPreferences.getString("token", "null"),
                        new AuctionTask.getAuctionCallBack() {
                            @Override
                            public void onSucces(Auction rawAuction) {
                                loadAuction();
                            }
                            @Override
                            public void onFail(String msg) {

                            }
                        });
            }
        });

    }

    private void loadAuction() {
        auctionTask.getAuctionsBid(auctionId, new AuctionTask.getAuctionCallBack() {
            @Override
            public void onSucces(Auction rawAuction) {
                auctionBids.clear();
                bidRVAdapter.notifyDataSetChanged();
                auction = null;
                auction = rawAuction;
                auctionBids.addAll(auction.getAuctionBids());
                bidRVAdapter.notifyDataSetChanged();

            }
            @Override
            public void onFail(String msg) {}
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        bidsRecyclerView = (RecyclerView) findViewById(R.id.bidsRecyclerView);
        bidRVAdapter = new doneSellsRecyclerVAdapter(auctionBids);
        layoutManager = new LinearLayoutManager(this);

        bidsRecyclerView.setLayoutManager(layoutManager);
        bidsRecyclerView.setAdapter(bidRVAdapter);

    }
}
