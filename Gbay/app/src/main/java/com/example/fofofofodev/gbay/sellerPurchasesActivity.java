package com.example.fofofofodev.gbay;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;

import com.example.fofofofodev.gbay.Adapters.PurchasesRecyclerViewAdapter;
import Tasks.SellerTask;
import models.Purchase;


public class sellerPurchasesActivity extends AppCompatActivity {

    String token;
    String userId;

    SellerTask sellerTask;
    ArrayList<Purchase> purchases = new ArrayList<>();

    RecyclerView purchasesRecyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_purchases);

        SharedPreferences sharedPreferences = getApplication().getSharedPreferences(getString(R.string.sharedPreferencesFileName), Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", null);
        userId = sharedPreferences.getString("userId", null);
        getSupportActionBar().setTitle(sharedPreferences.getString("userName", null));

        sellerTask = new SellerTask(this);

        sellerTask.getPurchases(token, userId, new SellerTask.getPurchasesCallback() {
            @Override
            public void OnSuccess(ArrayList<Purchase> RawPurchases) {
                purchases.addAll(RawPurchases);
                adapter.notifyDataSetChanged();
            }
            @Override
            public void onFail(String msg) {
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        purchasesRecyclerView = (RecyclerView) findViewById(R.id.purchasesREcyclerView);
        adapter = new PurchasesRecyclerViewAdapter(purchases);
        layoutManager = new LinearLayoutManager(this);
        purchasesRecyclerView.setLayoutManager(layoutManager);
        purchasesRecyclerView.setAdapter(adapter);
    }


    @Override
    protected void onResume() {
        super.onResume();



    }
}
