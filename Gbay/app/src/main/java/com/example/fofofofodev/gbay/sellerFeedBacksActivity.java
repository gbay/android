package com.example.fofofofodev.gbay;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.fofofofodev.gbay.R;

import Tasks.SellerTask;
import models.Purchase;

public class sellerFeedBacksActivity extends AppCompatActivity {

    EditText feedBackNote;
    EditText feedBackComment;
    Button editOrCreateFeedBack;
    Button DeleteFeedBackBtn;

    SellerTask sellerTask;


    String userId;
    String token;
    String sellId;

    Purchase purchase;

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_feed_backs);

        sharedPreferences = getApplication().getSharedPreferences(getString(R.string.sharedPreferencesFileName), Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", null);
        userId = sharedPreferences.getString("userId", null);
        getSupportActionBar().setTitle(getSharedPreferences(getString(R.string.sharedPreferencesFileName), Context.MODE_PRIVATE).getString("userName", null));

        sellerTask = new SellerTask(this);


        editOrCreateFeedBack = (Button) findViewById(R.id.editOrCreateFeedbackBtn);
        feedBackNote = (EditText) findViewById(R.id.NoteEditTextV);
        feedBackComment = (EditText) findViewById(R.id.commentEditTextV);
        DeleteFeedBackBtn = (Button) findViewById(R.id.DeleteFeedBackBtn);


        feedBackNote.setText(getIntent().getStringExtra("note"));
        feedBackComment.setText(getIntent().getStringExtra("comment"));

        sellId = getIntent().getStringExtra("sellId");

        touchEditOrCreateFeedback();

        DeleteFeedBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sellerTask.deleteFeedBack(token, sellId, new SellerTask.addProductCallback() {
                    @Override
                    public void onSucess() {
                        startActivity(new Intent(getApplicationContext(), sellerPurchasesActivity.class));}
                    @Override
                    public void onFail(String msg) {}
                });
            }
        });
    }

    private void touchEditOrCreateFeedback() {
        editOrCreateFeedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                purchase = new Purchase();
                purchase.setNote(Double.valueOf(feedBackNote.getText().toString()));
                purchase.setComment(feedBackComment.getText().toString());
                switch (getIntent().getStringExtra("buttonValue").toString()){
                    case "MODIFIER":
                        sellerTask.updateFeedback(token, sellId, purchase, new SellerTask.addProductCallback() {
                            @Override
                            public void onSucess() {
                                startActivity(new Intent(getApplicationContext(), sellerPurchasesActivity.class));
                            }

                            @Override
                            public void onFail(String msg) {

                            }
                        });
                        break;
                    case "AJOUTER UN FEEDBACK":
                        sellerTask.addFeedBack(token, sellId, purchase, new SellerTask.addProductCallback() {
                            @Override
                            public void onSucess() {startActivity(new Intent(getApplicationContext(), sellerPurchasesActivity.class));}
                            @Override
                            public void onFail(String msg) {}
                        });
                        break;
                    default:
                        break;
                }
            }
        });
    }
}
