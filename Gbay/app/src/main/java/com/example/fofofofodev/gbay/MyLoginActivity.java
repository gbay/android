package com.example.fofofofodev.gbay;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import Tasks.AuthTask;
import models.Seller;

public class MyLoginActivity extends AppCompatActivity {

    LinearLayout registerLayout;
    LinearLayout loginLayout;


    Button loginBtn;
    Button registerBtn;

    Button validateRegisterBtn;
    EditText newUserNameEditText;
    EditText newUserLoginEditText;
    EditText newUserPasswordEditText;

    TextView loginTV;
    TextView loginPassTV;

    private String token;
    private String loggedUserId;

    AuthTask authTask;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        authTask = new AuthTask(this);



        registerLayout = (LinearLayout) findViewById(R.id.registerLayout);
        registerLayout.setVisibility(View.INVISIBLE);


        handleRegisterButton();


        handleLoginBtn();


        validateRegisterBtn = (Button) findViewById(R.id.validateRegisterBtn);
        newUserNameEditText = (EditText) findViewById(R.id.newUsername);
        newUserLoginEditText = (EditText) findViewById(R.id.newUserLogin);
        newUserPasswordEditText = (EditText) findViewById(R.id.newUserPassword);



        validateRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Seller seller = new Seller();
                seller.setName(newUserNameEditText.getText().toString());
                seller.setEmail(newUserLoginEditText.getText().toString());
                seller.setPassword(newUserPasswordEditText.getText().toString());

                authTask.register(seller, new AuthTask.registerCallBack() {
                    @Override
                    public void onSuccess(boolean result) {
                        if(result){
                            Toast.makeText(MyLoginActivity.this,"Insciption réussie. Vous pouvez vous connecter", Toast.LENGTH_LONG).show();
                            registerLayout.setVisibility(View.INVISIBLE);
                            loginLayout.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFail(boolean result) {
                        Toast.makeText(MyLoginActivity.this,"Echec de l'inscription. Veuillez réessayer", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });


    }

    private void handleRegisterButton() {
        registerBtn = (Button) findViewById(R.id.goToRegister);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginLayout = (LinearLayout) findViewById(R.id.loginLayout);
                loginLayout.setVisibility(View.INVISIBLE);
                registerLayout.setVisibility(View.VISIBLE);

            }
        });
    }

    private void handleLoginBtn() {
        loginTV = (TextView) findViewById(R.id.loginTV);
        loginPassTV = (TextView) findViewById(R.id.loginPassTV);
        loginBtn = (Button) findViewById(R.id.validateLoginBtn);


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                models.Login login = new models.Login();
                login.setEmail(loginTV.getText().toString());
                login.setPassword(loginPassTV.getText().toString());

                authTask.requestForToken(login, new AuthTask.getTokenCallBack() {
                    @Override
                    public void onSuccess(Seller aSeller) {

                        loggedUserId =  String.valueOf(aSeller.getId());

                        token = aSeller.getToken();

                        Intent MainIntent = new Intent();
                        MainIntent.putExtra("token",token);
                        MainIntent.putExtra("loggedUserId", loggedUserId);

                        setResult(RESULT_OK, MainIntent);
                        finish();
                    }

                    @Override
                    public void onFail(String msg) {
                        Toast.makeText(MyLoginActivity.this,"Echec de l'authentification. Merci de vous inscrire", Toast.LENGTH_LONG).show();
                    }
                });
            }

        });
    }
}
