package com.example.fofofofodev.gbay.Adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.fofofofodev.gbay.ProductDetailsActivity;
import com.example.fofofofodev.gbay.R;

import java.util.ArrayList;

import models.Product;

/**
 * Created by fofofofodev on 30/03/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    ArrayList<Product> productsArray = new ArrayList<>();

    public RecyclerAdapter(ArrayList<Product> products){

        this.productsArray = products;
    }

    public void setProductsArray(ArrayList<Product> productsArray) {
        this.productsArray = productsArray;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item,parent,false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.productNameTextView.setText(productsArray.get(position).getName());
        holder.basePriceTV.setText(String.valueOf(productsArray.get(position).getPrice()));
        holder.productDescTV.setText(productsArray.get(position).getDescription());
        holder.productCategoriesTv.setText(productsArray.get(position).CategsToString());
        holder.productSellerNameTV.setText(productsArray.get(position).getSeller().getName());


        if(!productsArray.get(position).isDirectBuy()){
            holder.buyOrBidBtn.setText("VOIR LES ENCHERES");
        }

        holder.buyOrBidBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ProductDetailsActivity.class);
                intent.putExtra("productId", String.valueOf(productsArray.get(position).getId()));
                v.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return productsArray.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView productNameTextView;
        TextView productDescTV;
        TextView basePriceTV;
        //TextView auctionStartTV;
        //TextView auctionEndTV;
        TextView productCategoriesTv;
        Button buyOrBidBtn;
        TextView productSellerNameTV;


        public MyViewHolder(View itemView) {

            super(itemView);
            productNameTextView = (TextView) itemView.findViewById(R.id.productNameTextView);
            basePriceTV = (TextView) itemView.findViewById(R.id.basePriceTV);
            productDescTV = (TextView) itemView.findViewById(R.id.descriptionTV);
            productCategoriesTv = (TextView) itemView.findViewById(R.id.productCategoriesTextView);
            //auctionStartTV = (TextView) itemView.findViewById(R.id.auctionStartTV);
            //auctionEndTV = (TextView) itemView.findViewById(R.id.auctionEndTV);
            buyOrBidBtn = (Button) itemView.findViewById(R.id.buyOrBidBtn);
            productSellerNameTV = (TextView) itemView.findViewById(R.id.productSellerNameTV);

        }
    }


}
